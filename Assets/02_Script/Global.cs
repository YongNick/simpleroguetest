﻿using UnityEngine;
using System.Collections;

namespace GloClass
{
    // 현재 보드마스터에서만 사용하지만, 다른 곳에서도 사용할 지 몰라 글로벌로 설정.
	public class Counting
	{
		private int m_Max ;
		private int m_Min ;

		public int GetMax
		{
			get{ return m_Max;}
		}
		public int GetMin
		{
			get{ return m_Min;}
		}

		public Counting(int min,int max)
		{
			m_Max = max;
			m_Min = min;
		}
	}
}