﻿using UnityEngine;
using System;
using System.Collections.Generic; 		//Allows us to use Lists.
using Random = UnityEngine.Random;
using GloClass;

public class BoardManager : MonoBehaviour 
{


	// 가로,세로 줄 개수, 데이터 파일 추가후 고정수치 삭제.
	private int m_Columns = 8;
	private int m_Rows = 8;

	// 맵에 추가될 아이템,벽의 개수, 최소,최대개수 마찬가지로 데이터 파일 추가 후 init에서 변경.
	private Counting m_WallCount = new Counting (5, 9);
	private Counting m_FoodCount = new Counting (1, 5);
    
    // 타일들
	private GameObject m_Exit;
	private GameObject[] m_FloorTiles;
	private GameObject[] m_WallTiles;
    private GameObject[] m_FoodTiles;
	private GameObject[] m_EnemyTiles;
	private GameObject[] m_OuterWallTiles;

    // 타일 생성시 더러워지는 하이라키 정리해줄 오브젝트
    private Transform m_BoardHolder;
    // 랜덤생성위치 설정을 위해 맵의 좌표를 저장해 둘 리스트
    private List<Vector2> m_GridPositions = new List<Vector2>();

    void Init()
    {
        // 타일 초기화
        m_FloorTiles = new GameObject[8];
        for(int i=1; i<9; i++)
        {
            string First = "Floor";
            string Last = i.ToString();
            m_FloorTiles[i-1] = Resources.Load(First + Last) as GameObject;
        }
        m_OuterWallTiles = new GameObject[3];
        for (int i = 1; i < 4; i++)
        {
            string First = "OuterWall";
            string Last = i.ToString();
            m_OuterWallTiles[i - 1] = Resources.Load(First + Last) as GameObject;
        }
        m_WallTiles = new GameObject[8];
        for (int i = 1; i < 9; i++)
        {
            string First = "Wall";
            string Last = i.ToString();
            m_WallTiles[i - 1] = Resources.Load(First + Last) as GameObject;
        }

        // 리스트 정리
        m_GridPositions.Clear();

        for(int x = 1; x < GetColumn -1; x++)
        {
            for(int y = 1; y < GetRow - 1; y++)
            {
                m_GridPositions.Add(new Vector2(x, y));

            }
        }

        // 제대로 된 실행여부 확인을 위한 함수실행. 상위 매니저를 이용해 보드매니저를 사용할 것임.
        BoardSetUp();
        CreateAtRandom(m_WallTiles, 5, 10);
    }

    // 외벽/밑 바닥 생성
    void BoardSetUp()
    {
        m_BoardHolder = new GameObject("Board").transform;
        for(int x= -1; x<GetColumn +1; x++)
        {
            for (int y = -1; y < GetRow + 1; y++)
            {
                GameObject TempToInstantiate;

                if ( x == -1 || y == -1 || x == 8 || y== 8 )
                {
                    TempToInstantiate = m_OuterWallTiles[Random.Range(0, m_OuterWallTiles.Length)];
                }
                else
                {
                    TempToInstantiate = m_FloorTiles[Random.Range(0, m_FloorTiles.Length)];
                }
                GameObject instance = 
                (GameObject)Instantiate(TempToInstantiate, new Vector3(x, y, 0f),Quaternion.identity);
                instance.transform.parent = m_BoardHolder;

            }
        }
    }

    // 벽이나 아이템,적의 스폰위치를 랜덤으로 하기 위해 사용할 함수
    Vector2 RandomPosition()
    {
        int ranIndex = Random.Range(0, m_GridPositions.Count);

        Vector2 randomposition = m_GridPositions[ranIndex];

        m_GridPositions.RemoveAt(ranIndex);

        return randomposition;
    }

    // 랜덤으로 생성해주는 함수
    void CreateAtRandom(GameObject[] tileArray,int min,int max)
    {
        int objectCount = Random.Range(min, max);

        for(int i = 0; i<tileArray.Length; i++)
        {
            Vector2 randomPosition = RandomPosition();

            GameObject tileChoice = tileArray[Random.Range(0, tileArray.Length)];

            GameObject instance = 
            (GameObject)Instantiate(tileChoice, randomPosition, Quaternion.identity);

            instance.transform.parent = m_BoardHolder;
        }
    }

    void Start()
    {
        Init();
    }

    // Get
	public int GetColumn
	{
		get{ return m_Columns;}
	}
	public int GetRow
	{
		get{ return m_Rows; }
	}
	public Counting GetWallCount
	{
		get { return m_WallCount;}
	}
	public Counting GetFoodCount
	{
		get { return m_FoodCount;}
	}
}
